#ifndef SCENE_H
#define SCENE_H

#include <map>
#include <GL/glew.h>
#include <assimp/types.h>
#include <assimp/scene.h>
#include "math.h"

#define WIDTH 800
#define HEIGHT 480

/// aiScene have aiMeshes and aiAnimations
/// aiMesh have aiBones.
/// aiBones in aiMeshes point to aiNodes, which define the coordinate system.
/// An aiAnimation have an array of channels of type aiNodeAnim which
/// each animate an aiNode, including bones.

/// All this OpenGL data is constant during animation
struct MeshGLData {
    unsigned int vao;

    /// vertex buffer objects
    unsigned int vertices;
    unsigned int normals;
    unsigned int tangents;
    unsigned int bitangents;
    unsigned int tcoord0;
    unsigned int tcoord1;
    unsigned int tcoord2;
    unsigned int tcoord3;
    unsigned int indices; //vertex indices to glDrawElements 
    unsigned int boneIndices; //indices to bones affecting a vertex
    unsigned int weights; //bone weights
    unsigned int numElements; //number of faces * 3

    /* uniforms */
    //std::vector<aiMatrix4x4> bones; //final bones after transformation
};

struct AnimGLData;
struct Scene;

/// Abstract away rendering in AnimGLData so we can use
/// custom render functions and multiple passes
struct AnimRenderer {
    AnimRenderer();
    friend class AnimGLData;

protected:
    void drawBegin(unsigned int shader, int idx);
    void drawEnd(int idx);

private:
    void setParent(AnimGLData* parent);
    void setScene(const Scene* scene);

    void draw(int idx);
    int m_CurrentMesh;
    AnimGLData* m_Parent;
    const Scene* m_Scene;

    GLuint shader;
    GLuint texture;
    Matrix4f projection;
};    
    
/// This OpenGL data is dynamic during animation. This struct lets us
/// create multiple instances of an animation with different time offsets.
struct AnimGLData {
    friend class Scene;
    const Scene* m_Scene;

    //pointer to the animation data (constant)
    const aiAnimation* m_Animation;

    std::map<int, AnimRenderer*> m_Renderer;

    //2D array of uniform matrices for bones for every mesh (changes every frame)
    std::vector<std::vector<aiMatrix4x4> > m_Bones;

    // One worldspace matrix for every mesh
    std::vector<aiMatrix4x4> m_ModelView;

    float m_Time;
    aiMatrix4x4 m_Camera;
    
    int addRenderer(AnimRenderer* renderer, int modelIndex);
    void removeRenderer(int modelIndex);

    void stepAnimation(float t); //step one frame forwards
    void render(float t);
    void setCamera(const aiMatrix4x4& camera);

private:
    void recursiveUpdate(aiNode* node, const aiMatrix4x4& parentMatrix);
    void interpolateTranslation(const aiNodeAnim* nodeAnim, aiVector3D& translation);
    void interpolateScale(const aiNodeAnim* nodeAnim, aiVector3D& scale);
    void interpolateRotation(const aiNodeAnim* nodeAnim, aiQuaternion& rotation);
    
};

/// Could have used an std::pair, but a new type is more readable.
/// This struct is stored per-node if it is a bone, so we can look up
/// the model and bone index */
struct NodeMeshBoneIndex {
    int meshIndex;
    int boneIndex;
};

struct Scene {
    static const int MAX_UVMAPS = 4;
    static const int MAXBONESPERVERTEX = 4;
    static const int MAXBONESPERMESH = 32;
    
    const aiScene* m_Scene;

    // look up animations by name
    std::map<std::string, const aiAnimation*> m_LUTAnimation;

    // look up bone ID and Mesh ID by node.
    // I.e aiNode* 'node' is the 'i'th bone in the 'j'th mesh.
    std::map<const aiNode*, std::vector<NodeMeshBoneIndex> > m_LUTBone;

    // Constant/static data used by OpenGL for each mesh
    std::vector<MeshGLData*> m_MeshData;

    // Dynamic animation data per animation instance that changes every
    // animation frame
    std::vector<AnimGLData*> m_AnimData;

    Scene(const std::string& path);
    ~Scene();
    const aiScene* getScene() const;
    const aiAnimation* getAnimation(const std::string& name) const;
    const MeshGLData* getMeshGLData(int idx) const;
    size_t getMeshCount(){ return m_MeshData.size(); }
    AnimGLData* createAnimation(const std::string& name, const aiMatrix4x4& camera);

private:
    void initGLModelData();
    void initGLBoneData(MeshGLData* gldata, int meshID);
};

#endif

// vim: set tabstop=4 shiftwidth=4 softtabstop=4 expandtab:
