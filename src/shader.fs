#version 130

uniform sampler2D sampler0;

in vec2 tcoord;
out vec4 colorOut;

void main() {
  colorOut = texture(sampler0, tcoord);
}
