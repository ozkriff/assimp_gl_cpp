#ifndef MATH_H_GUARD
#define MATH_H_GUARD

#include <algorithm>
#include <cmath>

struct Vector4f {
    float x, y, z, w;

    Vector4f() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) {}

    Vector4f(float a, float b, float c, float d = 1.0f) : x(a), y(b), z(c), w(d) {}

    Vector4f operator+(const Vector4f& v) const {
        return Vector4f(x + v.x, y + v.y, z + v.z);
    }
    Vector4f operator-(const Vector4f& v) const {
        return Vector4f(x - v.x, y - v.y, z - v.z);
    }

    Vector4f operator+(const float& v) const {
        return Vector4f(x+v, y+v, z+v);
    }
    Vector4f operator-(const float& v) const {
        return Vector4f(x-v, y-v, z-v);
    }
    Vector4f operator*(const float& v) const {
        return Vector4f(x*v, y*v, z*v);
    }
    Vector4f operator/(const float& v) const {
        return Vector4f(x/v, y/v, z/v);
    }

    Vector4f& operator+=(const Vector4f& v) {
        *this = *this + v;
        return *this;
    }
    Vector4f& operator-=(const Vector4f& v) {
        *this = *this - v;
        return *this;
    }

    Vector4f& operator+=(const float& v) {
        *this = *this + v;
        return *this;
    }
    Vector4f& operator-=(const float& v) {
        *this = *this - v;
        return *this;
    }
    Vector4f& operator*=(const float& v) {
        *this = *this * v;
        return *this;
    }
    Vector4f& operator/=(const float& v) {
        *this = *this / v;
        return *this;
    }

    bool operator<(const Vector4f v) {
        /* Sorry, it's just a hack so we don't have to create a functor to std::sort */
        return z > v.z;
    }

    float length() const {
        return std::sqrt(x*x + y*y + z*z);
    }

    Vector4f unit() const {
        float len = length();
        if(std::abs(len) < 1e-8f)
            return Vector4f(0,0,0);
        return *this / len;
    }

    void normalize() {
        *this = unit();
    }
};

struct Matrix4f {
    float m[16];

    Matrix4f() {
        std::fill(m, m+16, 0.0f);
    }

    Matrix4f(
        const Vector4f& c1,
        const Vector4f& c2,
        const Vector4f& c3,
        const Vector4f& c4
    ) {
        m[0] = c1.x;
        m[1] = c1.y;
        m[2] = c1.z;
        m[3] = c1.w;
        m[4] = c2.x;
        m[5] = c2.y;
        m[6] = c2.z;
        m[7] = c2.w;
        m[8] = c3.x;
        m[9] = c3.y;
        m[10] = c3.z;
        m[11] = c3.w;
        m[12] = c4.x;
        m[13] = c4.y;
        m[14] = c4.z;
        m[15] = c4.w;
    }

    const float& operator[](size_t index) const {
        return m[index];
    }

    float& operator[](size_t index) {
        return m[index];
    }

    const float* c_ptr() const { return m; }
};

const float PI = 3.1415926535897932384626433832f;

inline float degtorad(float deg) {
    return deg / 180.0f * PI;
}

inline Matrix4f perspective(float fov, float aspect, float near, float far) {
    // Restrict fov to 179 degrees, for numerical stability
    if (fov >= 180.0f) {
        fov = 179.0f;
    }
    const float f = 1.0f / std::tan(degtorad(fov) * 0.5f);
    const float x = f;
    const float y = f * aspect;
    const float z1 = (far+near)/(near-far);
    const float z2 = (2.0f*far*near)/(near-far);
    return Matrix4f(
        Vector4f(x, 0, 0, 0),
        Vector4f(0, y, 0, 0),
        Vector4f(0, 0, z1, z2),
        Vector4f(0, 0, -1, 0)
    );
}

#endif

// vim: set tabstop=4 shiftwidth=4 softtabstop=4 expandtab:
