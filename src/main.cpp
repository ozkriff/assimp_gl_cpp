#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assimp/cimport.h>
#include <assimp/scene.h> 
#include <assimp/postprocess.h>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include <cstdio>
#include <algorithm>
#include <fstream>
#include <assimp/types.h>
#include "png_loader.h"
#include "glstuff.h"
#include "scene.h"

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        exit(0);
    }
}

int main(int argc, char* argv[]) {
    if (argc < 2 || argc > 3){
        printf("Usage: %s [COLLADA file] [Animation name]\n", argv[0]);
        return 0;
    }
    if (!glfwInit()){
        printf("Failed to initialize glfw\n");        
        return 0;
    }
    GLFWwindow* window = glfwCreateWindow(
        WIDTH, HEIGHT, "Optimus example", 0, NULL);
    if (!window) {
        glfwTerminate();
        printf("Failed to create glfw windows\n");
        return 0;
    }
    glfwSetKeyCallback(window, key_callback);    
    glfwMakeContextCurrent(window);
    if (glewInit()) {
        printf("Failed to init GL\n");
        glfwDestroyWindow(window);
        glfwTerminate();
        return 0;
    }
    std::string s(argv[1]);
    Scene scene(s);
    std::string animName("");
    if (argc == 3) {
        animName = std::string(argv[2]);
    }
    aiVector3D trans(0.0f, 0.0f, -2.0f);
    aiMatrix4x4 camera;
    aiMatrix4x4::Translation(trans, camera);
    AnimGLData* animation = scene.createAnimation(animName, camera);
    if (!animation) {
        printf("Couldn't find animation \"%s\".\n", animName.c_str());
        glfwDestroyWindow(window);
        glfwTerminate();
        return 0;
    }
    AnimRenderer* renderer = new AnimRenderer;
    for(size_t i = 0; i < scene.getMeshCount(); ++i){
        animation->addRenderer(renderer, i);
    }
    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        float t = glfwGetTime();
        animation->render(t);
        glfwSwapBuffers(window);
        if (t*32.0f >= 190.0f) {
            glfwSetTime(0.0f);
        }
    }
    glfwDestroyWindow(window);
    glfwTerminate();
    return 0;
}

// vim: set tabstop=4 shiftwidth=4 softtabstop=4 expandtab:
