#ifndef PNG_LOADER_H
#define PNG_LOADER_H

#include <string>
#include <vector>

bool LoadImagePNG(
    const std::string& name,
    std::vector<unsigned int>& buffer,
    unsigned int& width,
    unsigned int& height
);

#endif

// vim: set tabstop=4 shiftwidth=4 softtabstop=4 expandtab:
